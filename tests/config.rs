use std::{
    fs::OpenOptions,
    io::{BufReader, Read},
    path::PathBuf,
    sync::{Mutex, OnceLock},
    time::Duration,
};

use serde::Deserialize;
use tracing::error;

const fn default_configure_timeout() -> Duration {
    Duration::from_secs(10)
}

#[derive(Debug, Deserialize, Default)]
struct WayAssayConfig {
    #[serde(default = "default_configure_timeout")]
    configure_timeout: Duration,
}

static CONF: OnceLock<Mutex<WayAssayConfig>> = OnceLock::new();
fn configuration() -> &'static Mutex<WayAssayConfig> {
    CONF.get_or_init(|| Mutex::new(WayAssayConfig::default()))
}

fn set_config(cfg: WayAssayConfig) {
    CONF.get_or_init(|| Mutex::new(cfg));
}

pub fn configure_timeout() -> Duration {
    configuration().lock().unwrap().configure_timeout
}

pub fn build_config(cfg: &PathBuf) -> anyhow::Result<()> {
    let file = OpenOptions::new().read(true).open(cfg)?;

    let mut configuration = String::new();
    BufReader::new(file).read_to_string(&mut configuration)?;
    match ron::from_str::<WayAssayConfig>(&configuration) {
        Ok(parsed) => set_config(parsed),
        Err(e) => error!("Couldn't parse config at {cfg:?} ({e})"),
    }

    Ok(())
}
