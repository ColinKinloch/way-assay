use std::sync::OnceLock;

use anyhow::Context;
use libtest_mimic::Arguments;

use tracing_subscriber::EnvFilter;
use way_assay::{
    harness::{test_harness::Harness, wlcs::WlcsConnection},
    test_common::EarlyParameters,
};

use crate::config::build_config;
#[cfg(feature = "xdg")]
use crate::tests::xdg::basic as xdg_basic;

mod config;
mod tests;

fn setup_tracing() -> anyhow::Result<()> {
    let subscriber = tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .with_test_writer()
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("Couldn't set up tracing subscriber")
}

static EARLY_PARAMETERS: OnceLock<EarlyParameters> = OnceLock::new();

#[cfg(feature = "wlcs")]
fn start_harness(params: &EarlyParameters) -> anyhow::Result<impl Harness> {
    let wlcs = WlcsConnection::new(&params.suite_path().to_string_lossy())?;
    wlcs.start();
    Ok(wlcs)
}
#[cfg(not(feature = "wlcs"))]
fn start_harness(_params: &EarlyParameters) -> impl Harness {
    use way_assay::test_harness::NullHarness;

    NullHarness
}

#[cfg(feature = "wlcs")]
fn stop_harness(server: &impl Harness) -> anyhow::Result<()> {
    server
        .as_any()
        .downcast_ref::<WlcsConnection>()
        .context("Failed to downcast WlcsConnection")?
        .stop();
    Ok(())
}
#[cfg(not(feature = "wlcs"))]
fn stop_harness(_server: &impl Harness) {}

fn main() -> Result<(), anyhow::Error> {
    let args = Arguments::from_args();
    let mut tests = Vec::new();
    let params = EarlyParameters::new().context("Couldn't initialize parameters")?;
    let params = EARLY_PARAMETERS.get_or_init(|| params);
    if let Some(cfg) = params.cfg_path() {
        build_config(cfg).context("Failed to access config file")?;
    }
    let server = start_harness(params)?;
    tests::core::add_tests(&mut tests, server.clone());
    // xdg_basic::add_tests(&mut tests, wlcs.clone());
    #[cfg(feature = "xdg")]
    xdg_basic::add_tests(&mut tests, server.clone());
    // xdg_advanced::add_tests(&mut tests, wlcs.clone());

    let _trace = setup_tracing();

    let conclusion = libtest_mimic::run(&args, tests);

    println!("{conclusion:?}");
    conclusion.exit_if_failed();

    let _ = stop_harness(&server);

    Ok(())
}
