use libtest_mimic::Failed;
use way_assay::test_common::{Client, WssyError};
use wayland_backend::client::WaylandError;
use wayland_client::DispatchError;

pub mod core;
#[cfg(feature = "xdg")]
pub mod xdg;

#[macro_export]
macro_rules! to_name {
    ($obj:ident) => {
        $obj::interface().name
    };
}

#[macro_export]
macro_rules! push_test {
    ($tests:ident, $harness:ident, $fn:ident) => {
        let h = $harness.clone();
        $tests.push(Trial::skippable_test(stringify!($fn), move || $fn(h)));
    };
}

#[macro_export]
macro_rules! to_client {
    ($state:expr) => {
        match $state {
            Ok(c) => c,
            Err(WssyError::StateError(x)) => return $crate::skip_trial!(&x.to_string()),
            Err(e) => return Err(e.into()),
        }
    };
}

#[macro_export]
macro_rules! skip_trial {
    ($reason:expr) => {
        Ok(Some($reason.to_string()))
    };
}

type LibtestReturn = Option<String>;

const fn _pass() -> LibtestReturn {
    None
}

/// Successful result for a Trial
const fn pass() -> Result<LibtestReturn, Failed> {
    Ok(_pass())
}

/// Helper to turn bool into a Trial result
fn bool_result(result: bool, msg: &str) -> Result<LibtestReturn, Failed> {
    result.then_some(_pass()).ok_or(Failed::from(msg))
}

/// Helper to turn roundtrip into a Trial result.
fn roundtrip_result(client: &mut Client) -> Result<LibtestReturn, Failed> {
    client.roundtrip().map(|_| _pass()).map_err(Failed::from)
}

fn expect_failure(res: Result<usize, WssyError>, msg: &str) -> Result<LibtestReturn, Failed> {
    match res {
        Ok(_) => Err(Failed::from(msg)),
        Err(_) => pass(),
    }
}

fn expect_failure_code(
    res: Result<usize, WssyError>,
    name: &str,
    code: u32,
    msg: &str,
) -> Result<LibtestReturn, Failed> {
    match res {
        Ok(_) => Err(Failed::from(msg)),
        Err(WssyError::DispatchError(e)) => match e {
            DispatchError::Backend(e) => match e {
                WaylandError::Io(_) => todo!(),
                WaylandError::Protocol(e) => {
                    if e.object_interface == name && e.code == code {
                        pass()
                    } else {
                        Err(Failed::from(format!("Wrong protocol error {e:#?}")))
                    }
                }
            },
            _ => Err(Failed::from(msg)),
        },
        Err(e) => Err(Failed::from(format!("Unexpected error {}", e))),
    }
}
