use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc,
};

use libtest_mimic::{Failed, Trial};

use way_assay::{
    harness::test_harness::Harness,
    test_common::{core::CoreState, Client, RequestVersion, WssyError, WssyProtocol},
};

use wayland_client::{
    protocol::{
        wl_compositor::WlCompositor,
        wl_registry::{Event, WlRegistry},
        wl_shm,
        wl_shm_pool::WlShmPool,
        wl_surface::{self, WlSurface},
    },
    Proxy,
};

use crate::{push_test, to_client, to_name};

use super::{bool_result, expect_failure_code, pass, roundtrip_result, LibtestReturn};

/// Tests that wl_compositor global is present, but do it... twice
fn wl_compositor_present<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + Sync,
{
    let count = Arc::new(AtomicUsize::new(0));
    let c = count.clone();
    let mut client = Client::new(harness);
    let connection = client.connection.clone();
    client.with_interface(WlRegistry::interface(), move |msg| {
        if let Ok((
            _,
            Event::Global {
                name: _,
                interface,
                version: _,
            },
        )) = WlRegistry::parse_event(&connection, msg.into())
        {
            if WlCompositor::interface().to_string() == interface
                && c.fetch_add(1, Ordering::Relaxed) == 1
            {}
        }
    });

    client.get_registry();
    let _ = client.roundtrip();

    // and do it again to make sure it's still there :)
    client.get_registry();
    let _ = client.roundtrip();

    bool_result(
        count.load(Ordering::SeqCst) == 2,
        "wl_compositor wasn't found",
    )
}

fn create_region<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state_mut::<CoreState>();
    core.compositor_create_region(10, 10, 10, 10)?;
    pass()
}

fn bad_buffer_stride<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let pool = core.shm_create_pool(32)?;
    let _buffer = core.shm_pool_create_buffer(&pool, 0, 1, 2, 3, wl_shm::Format::Argb8888)?;

    expect_failure_code(
        client.roundtrip(),
        to_name!(WlShmPool),
        wl_shm::Error::InvalidStride.into(),
        "Invalid sized buffer should generate error",
    )
}

fn basic_buffer<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let _buffer = core.create_buffer(200, 200)?;

    roundtrip_result(&mut client)
}

fn basic_surface<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    core.compositor_create_surface()?;

    roundtrip_result(&mut client)
}

fn basic_pixrect<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let surface = core.compositor_create_surface()?;
    let buffer = core.create_buffer(200, 200)?;

    surface.attach(Some(&buffer), 0, 0);

    roundtrip_result(&mut client)
}

fn shm_oom<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let mut client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let pool = core.shm_create_pool(32)?;
    let _buffer = core.shm_pool_create_buffer(&pool, 1, 8, 1, 32, wl_shm::Format::Argb8888)?;

    expect_failure_code(
        client.roundtrip(),
        to_name!(WlShmPool),
        wl_shm::Error::InvalidStride.into(),
        "Failed to receive invalid buffer size error",
    )
}

fn shm_formats_contain_8888<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let client = to_client!(Client::new(harness).with_state::<CoreState>(None));
    let core = client.state::<CoreState>();

    let formats = core.shm_state.formats.lock()?;

    bool_result(
        formats.contains(&wl_shm::Format::Argb8888),
        "Missing ARGB8888",
    )?;
    bool_result(
        formats.contains(&wl_shm::Format::Xrgb8888),
        "Missing XrGB8888",
    )
}

fn invalid_offset_on_attach<H>(harness: H) -> Result<LibtestReturn, Failed>
where
    H: Harness + 'static,
{
    let comp_v5: WssyProtocol = to_name!(WlCompositor).version(5);
    let mut client =
        to_client!(Client::new(harness).with_state::<CoreState>(Some(vec![comp_v5].into())));
    let core = client.state::<CoreState>();
    let surface = core.compositor_create_surface()?;
    let buffer = core.create_buffer(200, 200)?;

    surface.attach(Some(&buffer), 4, 4);
    expect_failure_code(
        client.roundtrip(),
        to_name!(WlSurface),
        wl_surface::Error::InvalidOffset.into(),
        "Failed to receive invalid offset error for v5 of protocol",
    )
}

pub fn add_tests<H: Harness + Send + Sync + Clone + 'static>(tests: &mut Vec<Trial>, harness: H) {
    push_test!(tests, harness, wl_compositor_present);
    push_test!(tests, harness, create_region);
    push_test!(tests, harness, bad_buffer_stride);
    push_test!(tests, harness, basic_buffer);
    push_test!(tests, harness, basic_surface);
    push_test!(tests, harness, basic_pixrect);
    push_test!(tests, harness, shm_oom);
    push_test!(tests, harness, shm_formats_contain_8888);
    push_test!(tests, harness, invalid_offset_on_attach);
}
