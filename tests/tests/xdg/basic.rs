use libtest_mimic::{Failed, Trial};
use std::sync::{Arc, Condvar, Mutex};
use way_assay::{
    harness::test_harness::Harness,
    test_common::{core::CoreState, stable::xdg::XdgStableState, Client},
};
use wayland_client::Proxy;
use wayland_protocols::xdg::shell::client::xdg_surface::{self, XdgSurface};

use crate::{
    config::configure_timeout,
    push_test,
    tests::{expect_failure, roundtrip_result, LibtestReturn},
};

// "Creating an xdg_surface from a wl_surface which has a buffer attached or
// committed is a client error, and any attempts by a client to attach or
// manipulate a buffer prior to the first xdg_surface.configure call must also
// be treated as errors."
fn xdg_surface_disallows_buffer_before_first_configure<H: Harness>(
    harness: H,
) -> Result<LibtestReturn, Failed> {
    let mut client = Client::new(harness)
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;
    let core = client.state::<CoreState>();
    let xdg = client.state::<XdgStableState>();

    let surface = core.compositor_create_surface()?;
    let _xdg_surface = xdg.get_xdg_surface(&surface)?;
    let buffer = core.create_buffer(32, 32)?;
    surface.attach(Some(&buffer), 0, 0);
    surface.commit();

    expect_failure(client.roundtrip(), "Buffer commit should fail")
}

fn xdg_toplevel_allows_buffer_after_first_configure<H: Harness>(
    harness: H,
) -> Result<LibtestReturn, Failed> {
    let mut client = Client::new(harness)
        .with_state::<CoreState>(None)?
        .with_state::<XdgStableState>(None)?;
    let connection = client.connection.clone();
    let core = client.state::<CoreState>();
    let xdg = client.state::<XdgStableState>();
    let serial = Arc::new((Mutex::new((false, 0u32)), Condvar::new()));
    let s = serial.clone();

    let surface = core.compositor_create_surface()?;
    let xdg_surface = xdg.get_xdg_surface(&surface)?;
    let _xdg_toplevel = xdg.get_xdg_toplevel(&xdg_surface)?;

    client.with_interface(XdgSurface::interface(), move |msg| {
        let (_, event) = XdgSurface::parse_event(&connection, msg.into()).unwrap();
        match event {
            xdg_surface::Event::Configure { serial } => {
                xdg_surface.ack_configure(serial);
                let (guard, cvar) = &*s;
                let mut ser = guard.lock().unwrap();
                *ser = (true, serial);
                cvar.notify_one();
            }
            _ => unreachable!(),
        }
    });

    // Commiting without a buffer after the role is assigned should kick off the
    // configure event.
    surface.commit();
    roundtrip_result(&mut client)?;

    // Configure can arrive at some future time due to compositor implementation
    // details.
    let (guard, cvar) = &*serial;
    let result = cvar.wait_timeout_while(guard.lock().unwrap(), configure_timeout(), |x| !x.0)?;
    if result.1.timed_out() {
        // timed-out without the condition ever evaluating to false.
        return Err(Failed::from("Timed out waiting for configure"));
    }

    // Reget core state to appease the borrow checker
    let core = client.state::<CoreState>();
    let buffer = core.create_buffer(32, 32)?;
    surface.attach(Some(&buffer), 0, 0);
    surface.commit();

    roundtrip_result(&mut client)
}

pub fn add_tests<H: Harness + Send + Sync + Clone + 'static>(tests: &mut Vec<Trial>, harness: H) {
    push_test!(
        tests,
        harness,
        xdg_surface_disallows_buffer_before_first_configure
    );
    push_test!(
        tests,
        harness,
        xdg_toplevel_allows_buffer_after_first_configure
    );
}
