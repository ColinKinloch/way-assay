//! Common functionality for XDG shell based testing.

use std::{
    os::unix::prelude::OwnedFd,
    sync::{Arc, Mutex},
};

use tracing::{info, warn};
use wayland_backend::{
    client::{Backend, InvalidId, ObjectData, ObjectId},
    protocol::{same_interface, Message},
};
use wayland_client::{
    protocol::{wl_registry, wl_surface::WlSurface},
    Connection, Proxy,
};
use wayland_protocols::xdg::shell::client::{
    xdg_surface::{self, XdgSurface},
    xdg_toplevel::XdgToplevel,
    xdg_wm_base::{self, XdgWmBase},
};

use crate::{
    protocol_bind,
    test_common::{
        do_callback, wssy_parse_tee, CallbackMap, Client, ClientState, WssyError,
        WssyRequiredProtocols,
    },
};

/// Representation of XDG state
#[derive(Debug, Default)]
struct WaylandXdgState(pub(crate) Option<XdgWmBase>);

#[derive(Default)]
struct WssyXdgState {
    cb: Arc<Mutex<CallbackMap>>,
}

impl ObjectData for WssyXdgState {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let conn = Connection::from_backend(backend.clone());
        let info = conn.object_info(msg.sender_id.clone()).unwrap();
        if same_interface(info.interface, XdgSurface::interface()) {
            let Ok((msg, xdg_surface, event)) = wssy_parse_tee::<XdgSurface>(&conn, msg) else {
                todo!()
            };
            if let Err(e) = do_callback(
                &self.cb.try_lock().unwrap(),
                info.interface,
                msg,
                xdg_surface,
                event,
            ) {
                warn!("Callback error {e}. Test will likely fail");
            }
        } else if same_interface(info.interface, XdgToplevel::interface()) {
            let Ok((msg, xdg_surface, event)) = wssy_parse_tee::<XdgToplevel>(&conn, msg) else {
                todo!()
            };
            if let Err(e) = do_callback(
                &self.cb.try_lock().unwrap(),
                info.interface,
                msg,
                xdg_surface,
                event,
            ) {
                warn!("Callback error {e}. Test will likely fail");
            }
        } else {
            unreachable!()
        };
        None
    }

    fn destroyed(&self, _: ObjectId) {
        todo!()
    }
}

pub struct XdgStableState {
    pub xdg_wm_base: Option<XdgWmBase>,
    cbs: Arc<Mutex<CallbackMap>>,
}

impl ClientState for XdgStableState {
    fn new(
        client: &mut Client,
        protocol: Option<WssyRequiredProtocols>,
    ) -> Result<Self, WssyError> {
        let registry = client.globals.registry();
        let contents = client.globals.contents();
        let mut state = Self {
            xdg_wm_base: None,
            cbs: client.cbs.clone(),
        };
        let mut required_globals = protocol.unwrap_or_default();

        contents.with_list(|globals| {
            for global in globals.iter() {
                if &global.interface[..] == "xdg_wm_base" {
                    let (w, proto) = protocol_bind!(
                        registry,
                        XdgWmBase,
                        global,
                        required_globals,
                        Arc::new(WssyXdgState::default())
                    );
                    if required_globals.extract_if_higher_protocol(&proto.into()) {
                        state.xdg_wm_base.replace(w);
                    }
                }
            }
        });

        Ok(state)
    }
}

impl XdgStableState {
    pub fn get_xdg_surface(&self, surface: &WlSurface) -> Result<XdgSurface, InvalidId> {
        let wm_base = self.xdg_wm_base.as_ref().unwrap();
        wm_base.send_constructor(
            xdg_wm_base::Request::GetXdgSurface {
                surface: surface.clone(),
            },
            Arc::new(WssyXdgState {
                cb: self.cbs.clone(),
            }),
        )
    }

    pub fn get_xdg_toplevel(&self, xdg_surface: &XdgSurface) -> Result<XdgToplevel, InvalidId> {
        xdg_surface.send_constructor(
            xdg_surface::Request::GetToplevel {},
            Arc::new(WssyXdgState {
                cb: self.cbs.clone(),
            }),
        )
    }
}
