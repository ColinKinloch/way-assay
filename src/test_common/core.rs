//! Common state management for Wayland core protocol

/// Default pool size for SHM pool.
const SHM_POOL_SIZE: i32 = 32_000_000;

use std::{
    collections::HashSet,
    os::fd::{AsRawFd, OwnedFd},
    sync::{Arc, Mutex},
};

use tracing::{error, info, instrument, trace};
use wayland_backend::{
    client::{Backend, InvalidId, ObjectData, ObjectId},
    protocol::Message,
};
use wayland_client::{
    protocol::{
        wl_buffer::WlBuffer,
        wl_compositor::{self, WlCompositor},
        wl_region::WlRegion,
        wl_registry,
        wl_shm::{self, WlShm},
        wl_shm_pool::{self, WlShmPool},
        wl_surface::WlSurface,
    },
    Connection, Proxy, WEnum,
};

use crate::protocol_bind;

use super::{Client, ClientState, EventlessState, WssyError, WssyRegion, WssyRequiredProtocols};

#[derive(Debug, Default)]
pub struct ShmState {
    pub formats: Mutex<HashSet<wl_shm::Format>>,
}

impl ObjectData for ShmState {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let conn = Connection::from_backend(backend.clone());
        let Ok((_, event)) = WlShm::parse_event(&conn, msg) else {
            error!("Bad event format");
            return None;
        };

        match event {
            wl_shm::Event::Format { format } => self
                .formats
                .lock()
                .unwrap()
                .insert(format.into_result().unwrap()),
            _ => unreachable!(),
        };

        None
    }

    fn destroyed(&self, _object_id: ObjectId) {
        todo!()
    }
}

pub struct CoreState {
    pub compositor: Option<WlCompositor>,
    pub shm: Option<WlShm>,
    pub shm_state: Arc<ShmState>,
}

impl ClientState for CoreState {
    fn new(
        client: &mut Client,
        protocols: Option<WssyRequiredProtocols>,
    ) -> Result<Self, WssyError> {
        let registry = client.globals.registry();
        let contents = client.globals.contents();
        let shm_state = Arc::new(ShmState {
            formats: Mutex::new(HashSet::new()),
        });
        let mut state = Self {
            compositor: None,
            shm: None,
            shm_state: shm_state.clone(),
        };

        let mut required_globals = protocols.unwrap_or_default();

        contents.with_list(|globals| {
            for global in globals.iter() {
                match &global.interface[..] {
                    // TODO: When stable: https://github.com/rust-lang/rust/pull/104087
                    "wl_compositor" => {
                        let (c, proto) = protocol_bind!(
                            registry,
                            WlCompositor,
                            global,
                            required_globals,
                            Arc::new(EventlessState)
                        );
                        if required_globals.extract_if_higher_protocol(&proto.into()) {
                            state.compositor.replace(c);
                        };
                    }
                    "wl_shm" => {
                        let (s, proto) = protocol_bind!(
                            registry,
                            WlShm,
                            global,
                            required_globals,
                            shm_state.clone()
                        );
                        if required_globals.extract_if_higher_protocol(&proto.into()) {
                            state.shm.replace(s);
                        }
                    }
                    _ => (),
                }
            }
        });

        // Roundtrip to get SHM formats
        client.roundtrip()?;

        if !required_globals.is_empty() {
            return Err(WssyError::StateError(required_globals));
        }

        Ok(state)
    }
}

impl CoreState {
    pub fn shm_create_pool(&self, size: i32) -> Result<WlShmPool, InvalidId> {
        let shm = self.shm.as_ref().unwrap();
        let file = memfd::MemfdOptions::default()
            .create("wayland shm pool")
            .unwrap();

        shm.send_constructor(
            wl_shm::Request::CreatePool {
                fd: unsafe { std::os::fd::BorrowedFd::borrow_raw(file.as_raw_fd()) },
                size,
            },
            Arc::new(EventlessState),
        )
    }

    pub fn shm_pool_create_buffer(
        &self,
        pool: &WlShmPool,
        offset: i32,
        width: i32,
        height: i32,
        stride: i32,
        format: wl_shm::Format,
    ) -> Result<WlBuffer, InvalidId> {
        let format: WEnum<wl_shm::Format> = wayland_client::WEnum::Value(format);
        pool.send_constructor(
            wl_shm_pool::Request::CreateBuffer {
                offset,
                width,
                height,
                stride,
                format,
            },
            Arc::new(EventlessState),
        )
    }

    /// Convenience function, debatable whether this should be here or in each
    /// test file or sub-test suite.
    pub fn create_buffer(&self, width: i32, height: i32) -> Result<WlBuffer, InvalidId> {
        let pool = self.shm_create_pool(width * height * 4)?;
        self.shm_pool_create_buffer(&pool, 0, width, height, width * 4, wl_shm::Format::Argb8888)
    }

    pub fn compositor_create_surface(&self) -> Result<WlSurface, InvalidId> {
        let compositor = self.compositor.as_ref().unwrap();
        compositor.send_constructor(
            wl_compositor::Request::CreateSurface {},
            Arc::new(EventlessState),
        )
    }

    #[instrument(skip(self), err, ret)]
    pub fn compositor_create_region(
        &mut self,
        x: i32,
        y: i32,
        width: i32,
        height: i32,
    ) -> Result<WlRegion, InvalidId> {
        trace!("Creating region");
        let compositor = self.compositor.as_ref().unwrap();
        compositor.send_constructor(
            wl_compositor::Request::CreateRegion {},
            Arc::new(WssyRegion::default()),
        )
    }
}
