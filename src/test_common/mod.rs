//! Set of consolidated helpers for creation of Wayland tests.
//!
//! The test framework is built around [wayland_client] but discards the notion
//! of [wayland_client::Dispatch] in favor of a more focused, and less verbose
//! dispatching mechanism ([WaylandTest::add_callback])
use std::{
    any::{Any, TypeId},
    cmp,
    collections::{HashMap, HashSet},
    default::Default,
    env,
    error::Error,
    fmt,
    ops::{Deref, DerefMut},
    os::fd::OwnedFd,
    path::PathBuf,
    sync::{atomic::AtomicBool, Arc, Mutex, RwLock},
};

use tracing::{debug, instrument, trace, warn, Level};
use wayland_backend::{
    client::{Backend, InvalidId, ObjectData, ObjectId},
    protocol::{self, same_interface, Argument, Interface, Message, INLINE_ARGS},
    smallvec::{self, smallvec},
};
use wayland_client::{
    globals::{self, GlobalList, GlobalListContents},
    protocol::{
        wl_display::{self, WlDisplay},
        wl_output::{self, Transform, WlOutput},
        wl_registry::{self, WlRegistry},
        wl_surface::{self, WlSurface},
    },
    Connection, Dispatch, DispatchError, EventQueue, Proxy, QueueHandle, WEnum, Weak,
};

use crate::harness::test_harness::Harness;

use self::rect::{Logical, Rectangle};

pub mod core;
mod rect;
pub mod stable;

/// Parameters for running running way-assay
///
/// These parameters should be environment variables.
#[derive(Clone)]
pub struct EarlyParameters {
    suite_path: PathBuf,
    cfg_path: Option<PathBuf>,
}

impl EarlyParameters {
    pub fn new() -> anyhow::Result<Self> {
        let lib = env::var("WSSY_LIB")?;
        let cfg = env::var("WSSY_CFG");
        Ok(EarlyParameters {
            suite_path: lib.into(),
            cfg_path: cfg.ok().map(Into::into),
        })
    }

    /// Get the path to the integration harness, ie. foowlcs.so
    pub fn suite_path(&self) -> &PathBuf {
        &self.suite_path
    }

    pub fn cfg_path(&self) -> Option<&PathBuf> {
        self.cfg_path.as_ref()
    }
}

#[macro_export]
macro_rules! protocol_bind {
    ($reg:expr, $proto:tt, $global:expr, $constraints:expr, $objdata:expr) => {{
        let global_str = paste::paste! { stringify!([<$proto:snake>]) };

        // Bind the specific version set by the caller of the protocol, or fallback to
        // the latest version supported by the server.
        let version: u32 = $constraints
            .iter()
            .find(|r| r.0 .0 == global_str)
            .map_or($global.version, { |w| w.0 .1 });

        (
            match $reg.send_constructor::<$proto>(
                wl_registry::Request::Bind {
                    name: $global.name,
                    id: ($proto::interface(), version),
                },
                $objdata,
            ) {
                Ok(y) => y,
                Err(e) => {
                    info!("Couldn't bind {} {e}", stringify!($proto));
                    continue;
                }
            },
            // Return the name as string, and version bound
            (global_str, version),
        )
    }};
}

pub trait RequestVersion {
    fn version(&'static self, version: u32) -> WssyProtocol;
}

impl RequestVersion for &str {
    fn version(&'static self, version: u32) -> WssyProtocol {
        WssyProtocol((self, version))
    }
}

#[derive(Debug, PartialEq)]
pub struct WssyProtocol((&'static str, u32));
#[derive(Debug, Default)]
pub struct WssyRequiredProtocols(Vec<WssyProtocol>);

impl WssyRequiredProtocols {
    /// Return true if the protocol version being tested is high enough. Filter
    /// out if so.
    fn extract_if_higher_protocol(&mut self, p: &WssyProtocol) -> bool {
        // No test for this specific protocol.
        if !self.iter().any(|proto| proto.0 .0 == p.0 .0) {
            return true;
        }

        // If the protocol is in the list, check that the versions match
        let l = self.0.len();
        self.0.retain(|g| g.0 .0 != p.0 .0 || p < g);
        assert!(l + 2 > self.0.len()); // More than one was removed
        l - 1 == self.0.len()
    }
}

impl fmt::Display for WssyRequiredProtocols {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.iter()
            .try_fold((), |_result, proto| writeln!(f, "{}", proto))
    }
}

impl From<Vec<WssyProtocol>> for WssyRequiredProtocols {
    fn from(value: Vec<WssyProtocol>) -> Self {
        Self(value)
    }
}

impl Deref for WssyRequiredProtocols {
    type Target = Vec<WssyProtocol>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for WssyRequiredProtocols {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.0.as_mut()
    }
}

impl fmt::Display for WssyProtocol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} v{}", self.0 .0, self.0 .1)
    }
}

impl PartialOrd for WssyProtocol {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        if self.0 .0 != other.0 .0 {
            None
        } else {
            Some(self.0 .1.cmp(&other.0 .1))
        }
    }
}

impl From<protocol::Interface> for WssyProtocol {
    fn from(value: protocol::Interface) -> Self {
        Self((value.name, value.version))
    }
}

impl From<&protocol::Interface> for WssyProtocol {
    fn from(value: &protocol::Interface) -> Self {
        Self((value.name, value.version))
    }
}

impl From<(&'static str, u32)> for WssyProtocol {
    fn from(value: (&'static str, u32)) -> Self {
        Self((value.0, value.1))
    }
}

/// Message from the Wayland server.
///
/// This is a container object over [`Message`] which is useful/necessary to be
/// able to clone. This message is clonable with the caveat that [`OwnedFd`]
/// clone may fail.
///
/// Message does implement [`Clone`], but not when [`OwnedFd`] is the generic.
#[derive(Debug)]
pub struct WssyMessage(Message<ObjectId, OwnedFd>);

impl fmt::Display for WssyMessage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} op {}", self.0.sender_id, self.0.opcode)
    }
}

impl WssyMessage {
    fn try_clone(&self) -> Result<Self, WssyError> {
        let mut new_args: smallvec::SmallVec<[Argument<ObjectId, OwnedFd>; INLINE_ARGS]> =
            smallvec![];
        for a in &self.0.args {
            new_args.push(match a {
                Argument::Fd(fd) => match fd.try_clone() {
                    Ok(f) => Argument::Fd(f),
                    Err(e) => return Err(WssyError::DupError(e.into())),
                },
                Argument::Int(i) => Argument::Int(*i),
                Argument::Uint(u) => Argument::Uint(*u),
                Argument::Fixed(f) => Argument::Fixed(*f),
                Argument::Str(s) => Argument::Str(s.clone()),
                Argument::Object(o) => Argument::Object(o.clone()),
                Argument::NewId(n) => Argument::NewId(n.clone()),
                Argument::Array(a) => Argument::Array(a.clone()),
            })
        }
        Ok(WssyMessage(Message::<ObjectId, OwnedFd> {
            sender_id: self.0.sender_id.clone(),
            opcode: self.0.opcode,
            args: new_args,
        }))
    }
}

impl From<WssyMessage> for Message<ObjectId, OwnedFd> {
    fn from(val: WssyMessage) -> Self {
        Message {
            sender_id: val.0.sender_id,
            opcode: val.0.opcode,
            args: val.0.args,
        }
    }
}

impl TryFrom<&Message<ObjectId, OwnedFd>> for WssyMessage {
    type Error = WssyError;

    fn try_from(value: &Message<ObjectId, OwnedFd>) -> Result<Self, Self::Error> {
        let mut new_args: smallvec::SmallVec<[Argument<ObjectId, OwnedFd>; INLINE_ARGS]> =
            smallvec![];
        for a in &value.args {
            new_args.push(match a {
                Argument::Fd(fd) => match fd.try_clone() {
                    Ok(f) => Argument::Fd(f),
                    Err(e) => return Err(WssyError::DupError(e.into())),
                },
                Argument::Int(i) => Argument::Int(*i),
                Argument::Uint(u) => Argument::Uint(*u),
                Argument::Fixed(f) => Argument::Fixed(*f),
                Argument::Str(s) => Argument::Str(s.clone()),
                Argument::Object(o) => Argument::Object(o.clone()),
                Argument::NewId(n) => Argument::NewId(n.clone()),
                Argument::Array(a) => Argument::Array(a.clone()),
            })
        }

        Ok(WssyMessage(Message::<ObjectId, OwnedFd> {
            sender_id: value.sender_id.clone(),
            opcode: value.opcode,
            args: new_args,
        }))
    }
}

/// Parses a [`Message`] and copies to a [`WssyMessage`]
///
/// # Errors
///
/// This function will return an error if the [`WssyMessage`] is unparseable.
pub fn wssy_parse_tee<I: Proxy>(
    conn: &Connection,
    msg: Message<ObjectId, OwnedFd>,
) -> Result<(WssyMessage, I, <I as Proxy>::Event), WssyError> {
    let iface = conn.object_info(msg.sender_id.clone())?.interface;
    if !same_interface(iface, I::interface()) {
        return Err(WssyError::InvalidIface {
            expected: I::interface().to_string(),
            found: iface.to_string(),
        });
    }
    let m2 = WssyMessage::try_from(&msg)?;
    Ok(I::parse_event(conn, msg).map(|(p, e)| (m2, p, e))?)
}

#[derive(Clone, Default)]
struct WssyRegion(Arc<Mutex<Vec<Rectangle<i32, Logical>>>>);

impl WssyRegion {
    fn add(&mut self, rect: Rectangle<i32, Logical>) {
        let mut guard = self.0.lock().unwrap();
        if guard.iter().any(|r| r.contains_rect(rect)) {
            return;
        }
        guard.push(rect)
    }

    fn subtract(&self, rect: Rectangle<i32, Logical>) {
        let guard = self.0.lock().unwrap();
        Rectangle::subtract_rects_many_in_place(guard.to_vec(), vec![rect]);
    }
}

impl ObjectData for WssyRegion {
    fn event(
        self: Arc<Self>,
        _backend: &Backend,
        _msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        unreachable!()
    }

    fn destroyed(&self, _object_id: ObjectId) {
        todo!()
    }
}

struct WssyBuffer(AtomicBool);

impl ObjectData for WssyBuffer {
    fn event(
        self: Arc<Self>,
        _backend: &Backend,
        _msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        None
    }

    fn destroyed(&self, _object_id: ObjectId) {}
}

struct WssySurfaceInner {
    preferred_scale: isize,
    preferred_xform: wl_output::Transform,
    outputs: HashSet<Weak<WlOutput>>,
}

#[derive(Clone)]
struct WssySurface {
    cbs: Arc<Mutex<CallbackMap>>,
    inner: Arc<Mutex<WssySurfaceInner>>,
}

impl WssySurface {
    fn add_output(&self, output: &WlOutput) {
        self.inner
            .lock()
            .unwrap()
            .outputs
            .insert(output.downgrade());
    }

    fn remove_output(&self, output: &WlOutput) {
        self.inner
            .lock()
            .unwrap()
            .outputs
            .remove(&output.downgrade());
    }

    fn set_scale(&self, factor: i32) {
        self.inner.lock().unwrap().preferred_scale = factor as isize;
    }

    fn set_transform(&self, transform: WEnum<Transform>) {
        let Ok(xform) = transform.into_result() else {
            warn!("Invalid transform");
            return;
        };
        self.inner.lock().unwrap().preferred_xform = xform;
    }
}

impl ObjectData for WssySurface {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let surf_msg =
            wssy_parse_tee::<WlSurface>(&Connection::from_backend(backend.clone()), msg).ok()?;
        match surf_msg.2 {
            wl_surface::Event::Enter { ref output } => self.add_output(output),
            wl_surface::Event::Leave { ref output } => self.remove_output(output),
            wl_surface::Event::PreferredBufferScale { factor } => self.set_scale(factor),
            wl_surface::Event::PreferredBufferTransform { transform } => {
                self.set_transform(transform)
            }
            _ => unreachable!(),
        };
        if let Err(e) = do_callback(
            &self.cbs.try_lock().unwrap(),
            WlSurface::interface(),
            surf_msg.0,
            surf_msg.1,
            surf_msg.2,
        ) {
            warn!("Callback error {e}. Test will likely fail");
        }
        None
    }

    fn destroyed(&self, _object_id: ObjectId) {
        todo!()
    }
}

#[derive(Default)]
struct EventlessState;

impl ObjectData for EventlessState {
    fn event(
        self: Arc<Self>,
        _backend: &Backend,
        _msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        None
    }

    fn destroyed(&self, object_id: ObjectId) {
        trace!("Destroyed {} (id: {object_id})", object_id.interface())
    }
}

#[derive(Debug)]
/// Data passed to callbacks and used for synchronization events
pub struct TestData;

type WssyCallback = dyn Fn(WssyMessage) + Send;
#[derive(Default)]
struct CallbackMap {
    callbacks: HashMap<(&'static str, i32), Vec<Box<WssyCallback>>>,
}

impl std::fmt::Debug for CallbackMap {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("CallbackMap")
            .field("callbacks", &self.callbacks.keys())
            .finish()
    }
}

/// Helper for running Wayland protocol tests
///
/// While individual tests may want to bind to specific globals, the helpers
/// rely on also having access to these. [`WaylandTest`] provides the ability to
/// register for callbacks from the events and also track enough state to be
/// useful.
///
/// This is the lowest level of test helper. If too much is automatigically
/// done, the internal queues may need to be used directly.
pub struct Client {
    pub connection: Connection,
    /// The display singleton.
    display: WlDisplay,

    q: EventQueue<TestData>,

    /// Data passed to individual tests and available from within the test.
    ///
    /// This structure is largely opaque to test writers. Generally the pass,
    /// fail, or wait functionality is all that is needed.
    test_data: Arc<RwLock<TestData>>,

    /// Callbacks registered by the test client for a given interface.
    cbs: Arc<Mutex<CallbackMap>>,
    globals: GlobalList,

    states: HashMap<std::any::TypeId, Box<dyn Any>>,
}

impl Dispatch<wl_registry::WlRegistry, GlobalListContents> for Client {
    fn event(
        _state: &mut Self,
        _registry: &wl_registry::WlRegistry,
        _event: <wl_registry::WlRegistry as wayland_client::Proxy>::Event,
        _data: &GlobalListContents,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
    ) {
    }
}

pub trait ClientState: Any {
    /// Instantiate new client state
    fn new(
        client: &mut Client,
        protocols: Option<WssyRequiredProtocols>,
    ) -> Result<Self, WssyError>
    where
        Self: Sized;
}

struct RegistryState {
    cb: Arc<Mutex<CallbackMap>>,
}

impl ObjectData for RegistryState {
    fn event(
        self: Arc<Self>,
        backend: &Backend,
        msg: Message<ObjectId, OwnedFd>,
    ) -> Option<Arc<dyn ObjectData>> {
        let (wmsg, registry, event) =
            wssy_parse_tee::<WlRegistry>(&Connection::from_backend(backend.clone()), msg).unwrap();
        if let Err(e) = do_callback(
            &self.cb.try_lock().unwrap(),
            WlRegistry::interface(),
            wmsg,
            registry,
            event,
        ) {
            warn!("Callback error {e}. Test will likely fail");
        }

        // registry doesn't actually create any objects
        None
    }

    fn destroyed(&self, _object_id: ObjectId) {
        todo!()
    }
}

impl Client {
    /// Create a new test.
    pub fn new<H: Harness>(harness: H) -> Self {
        let connection = harness.connection().unwrap();
        let wl_display = connection.display();
        let q = connection.new_event_queue();
        let (globals, _) = globals::registry_queue_init::<Client>(&connection).unwrap();
        Self {
            connection,
            display: wl_display,
            q,
            test_data: Arc::new(RwLock::new(TestData)),
            cbs: Arc::new(Mutex::new(CallbackMap::default())),
            globals,
            states: HashMap::new(),
        }
    }

    pub fn get_registry(&mut self) -> WlRegistry {
        let cb = self.cbs.clone();
        let data = RegistryState { cb };
        let registry = self
            .display
            .send_constructor(wl_display::Request::GetRegistry {}, Arc::new(data))
            .unwrap();

        let _ = self.roundtrip();

        registry
    }

    pub fn with_state<S: ClientState>(
        mut self,
        protocols: Option<WssyRequiredProtocols>,
    ) -> Result<Self, WssyError> {
        let b = Box::new(S::new(&mut self, protocols)?);
        self.states.insert(TypeId::of::<S>(), b);
        Ok(self)
    }

    pub fn state<S: ClientState>(&self) -> &S {
        let anystate = self.states.get(&TypeId::of::<S>()).unwrap();
        anystate.as_ref().downcast_ref().unwrap()
    }

    pub fn state_mut<S: ClientState>(&mut self) -> &mut S {
        let anystate: &mut std::boxed::Box<dyn std::any::Any> =
            self.states.get_mut(&TypeId::of::<S>()).unwrap();
        anystate.as_mut().downcast_mut().unwrap()
    }

    /// Issue a synchronous roundtrip with the server.
    ///
    /// ```no_run
    /// use way_assay::{harness::test_harness::NullHarness, test_common::WaylandTestBuilder};
    /// let builder: WaylandTestBuilder<NullHarness> = todo!();
    /// let mut test = builder.create_test();
    /// test.roundtrip();
    /// ```
    #[instrument(skip(self), err(level = Level::TRACE))]
    pub fn roundtrip(&mut self) -> Result<usize, WssyError> {
        let mut td = self.test_data.try_write().unwrap();
        trace!("roundtrip");
        self.q.roundtrip(&mut td).map_err(WssyError::from)
    }

    /// Request a callback for the given Wayland protocol
    ///
    /// When using the test framework, it is often helpful to have the framework
    /// manage most of the Wayland protocol interaction and the individual
    /// test request only what is needed. [`Self::add_callback`] provides a way
    /// for the test to opt in, with a concise closure, only what
    /// it wants to opt in to.
    ///
    /// ```no_run
    /// use std::sync::{
    ///     atomic::{AtomicUsize, Ordering},
    ///     Arc,
    /// };
    /// use way_assay::{
    ///     harness::test_harness::{Harness, NullHarness},
    ///     test_common::WaylandTestBuilder,
    /// };
    /// use wayland_client::{protocol::wl_registry::WlRegistry, Proxy};
    ///
    /// let count = Arc::new(AtomicUsize::new(0));
    /// let c = count.clone();
    /// let builder: WaylandTestBuilder<NullHarness> = todo!();
    /// let mut test = builder.create_test();
    /// test.add_callback(WlRegistry::interface(), move |msg| {
    ///     c.fetch_add(1, Ordering::Relaxed);
    /// });
    /// println!("{} globals advertised", count.load(Ordering::SeqCst));
    /// ```
    #[instrument(skip(self, interface, f), fields(interface.name = %interface.name))]
    pub fn with_interface<F>(&self, interface: &Interface, f: F)
    where
        F: Fn(WssyMessage) + Send + 'static,
    {
        debug!("Adding callback");
        self.cbs
            .lock()
            .unwrap()
            .callbacks
            .entry((interface.name, interface.version as i32))
            .or_default()
            .push(Box::new(f));
    }

    #[instrument(skip(self, _f, _g))]
    pub fn with_global<F, G>(self, global: &str, _f: F, _g: Option<G>)
    where
        F: Fn(u32, &str, u32) + 'static,
        G: Fn(u32) + 'static,
    {
    }
}

/// Helper to initiate a callback for the given protocol.
///
/// This should be initiated from [`wayland_client::backend::ObjectData::event`]
#[instrument(skip(map, global, msg, _proxy, _event), fields(global.name = %global.name))]
fn do_callback<I: Proxy>(
    map: &CallbackMap,
    global: &Interface,
    msg: WssyMessage,
    _proxy: I,
    _event: <I as Proxy>::Event,
) -> Result<(), WssyError> {
    trace!("");
    let Some(callbacks) = map.callbacks.get(&(global.name, global.version as i32)) else {
        return Ok(());
    };

    for callback in callbacks.iter() {
        debug!("Dispatching callback");
        callback(msg.try_clone()?)
    }

    Ok(())
}

/// way-assay specific errors
///
/// The internal event queue will call into the [`ObjectData`] for the given
/// protocol. As part of this, an error might be generated.
#[derive(thiserror::Error, Debug)]
pub enum WssyError {
    #[error("Dispatch error ({0}) during roundtrip")]
    DispatchError(#[from] DispatchError),

    #[error("Invalid Id ({0}) for operation")]
    InvalidId(#[from] InvalidId),

    #[error("Invalid Interface (expected {expected:?}, found {found:?})")]
    InvalidIface { expected: String, found: String },

    #[error("Couldn't clone")]
    DupError(#[from] Box<dyn Error>),

    #[error("Couldn't initialize requested state. Missing protocols: {0:?}")]
    StateError(WssyRequiredProtocols),
}
