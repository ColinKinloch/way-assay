# way-assay

way: Wayland

Assay: "To examine a substance to find out how pure it is or what its properties (= qualities)"

Way-assay is a test suite for Wayland compositors which is meant to determine
spec conformance as well as some amount of
[functional](https://en.wikipedia.org/wiki/Functional_testing) correctness. It
is designed to be usable for any Wayland compositor, including ChromeOS's Exo.

The project is in very early development.

## Installation

Clone the repo.

### Dependencies

Cargo

## Temporary Caveat

For early development, way-assay uses [WLCS](https://github.com/MirServer/wlcs)
as its ABI.

## Usage

In early development, way-assay requires a WLCS implementation for the server.
This may be obtained by MIR, Anvil, or Sudbury. To run the current example for
[Sudbury](https://gitlab.freedesktop.org/bwidawsk/sudbury/):

```bash
RUST_LOG=off \ # Keep things quiet
SBRY_DRM_DEV=/dev/dri/card0 # Sudbury requires an env var to the DRM device node
WSSY_LIB=../sudbury/target/debug/libwlcs.so \ # WLCS implementation
cargo test --test wayassay
```

To build wlcs integration in Sudbury:

```bash
cargo wlcs-pixman-build
```

## Support

Use the gitlab issue tracker, or message me directly on IRC (libera or oftc),
or matrix. I am *bwidawsk*

## Roadmap

See [dev README](/README_dev.md) for information geared toward developers.

### Develop suitable ABI

- \[ \] Draft and create a suitable interface that can be used cross-compositors for
  test.
- \[ \] Implement ABI for way-assay
- \[ \] Implement ABI for Weston

### Investigate running in ChromeOS

ChromeOS doesn't include `cargo`. Determine the best solution

### Develop tests

- \[ \] Core tests
- \[ \] XDG tests
- \[ \] External tests

## Contributing

Informal for now, please run `cargo fmt` and `cargo clippy`

## License

MIT
